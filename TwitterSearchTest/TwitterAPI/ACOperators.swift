//
//  ACOperators.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-27.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import CoreGraphics

public protocol PrimitiveTypes{}
public protocol Literals{}

extension String: Literals{}
extension NSString: Literals{}
extension Int: PrimitiveTypes{}
extension Bool: PrimitiveTypes{}
extension Double: PrimitiveTypes{}
extension Float: PrimitiveTypes{}
extension CGFloat: PrimitiveTypes{}

precedencegroup ExponentiativePrecedence {
    associativity: right
    higherThan: MultiplicationPrecedence
}

infix operator <~ : ExponentiativePrecedence

private typealias ACDictionary = Dictionary<String, Any>

public func <~ <T>( property: inout T?, anyObject: String) {
    var newValue  = anyObject as? T
     mapToType(&newValue, anyObject: anyObject)
//    if property is NSDate {
//        property <~ anyObject
//    }
    property = newValue
}

public func <~ <T>( property: inout T, anyObject: String) {
    var newValue  = anyObject as? T
    mapToType(&newValue, anyObject: anyObject)
//    if property is NSDate {
//        property <~ anyObject
//    }
    guard let val = newValue else {
        return
    }
    property = val
}

public func <~ <T>( property: inout T?, anyObject: T?) {
    property = anyObject
}
public func <~ <T>( property: inout T, anyObject: T) {
    property = anyObject
}

public func <~ <T>( property: inout T?, anyObject: Any?) {
    property = anyObject as? T
}

public func <~ <T>( property: inout T, anyObject: Any?) {
    guard let anObject = anyObject as? T  else {
       return
    }
    property = anObject
}

public func <~ <T:PrimitiveTypes>( property: inout T, anyObject: Any?) {
    if let anObject = anyObject as? T {
        property = anObject
    } else {
        var newValue  = anyObject as? T
        mapToType(&newValue, anyObject: anyObject)

        guard let val = newValue  else {
            return
        }
        property = val
    }
}

public func <~ <T: PrimitiveTypes>( property: inout T?, anyObject: Any?) {
    var newValue  = anyObject as? T
    mapToType(&newValue, anyObject: anyObject)
    property = newValue
}

public func <~ <T: CustomFromStringConvertable>( property: inout T?, anyObject: Any) {
    if let stringObject = anyObject as? String {
        property = T(stringObject)
    }
}

public func <~ <T: ACMappableProtocol>(property: inout [T]?, anyObject: Any) {
    if let array = anyObject as? [ACDictionary] {
        var newProperty = [T]()
        for element in array {
            newProperty.append(T(dict: element))
        }
        property = newProperty
    }
}

public func <~ <T: ACMappableProtocol>(property: inout [T], anyObject: Any) {
    if let array = anyObject as? [ACDictionary] {
        var newProperty = [T]()
        for element in array {
            newProperty.append(T(dict: element))
        }
        property = newProperty
    }
}

public func <~ <T: ACMappableProtocol>( property: inout T, anyObject: Any) {
    if let dict = anyObject as? ACDictionary {
        property = T(dict: dict)
    }
}

public func <~ <T: ACMappableProtocol>( property: inout T?, anyObject: Any) {
    if let dict = anyObject as? ACDictionary {
        property = T(dict: dict)
    }
    else {
        property = nil
    }
}

public func <~ <T: PrimitiveTypes>( property: inout [T]?, anyObject: AnyObject?) {
    property = [T]()

    if let obj = anyObject as? [T] {
        property = obj
    } else if let anyArr = anyObject as? [AnyObject] {
        
        
        for element in anyArr {
            var nElement : T?
            nElement <~ element
                if let _ = nElement {
                    property?.append(nElement!)
                }
        }
    }
}

public func <~ <T>( property: inout [T], anyObject: AnyObject?) {
    if let obj = anyObject as? [String] {
        property <~ obj
    } else if let obj = anyObject as? [T] {
        property = obj
    }
}

public func <~( property: inout Date?, anyObject: Any) -> Date? {
    if let object = anyObject as? Date {
        property = object
    } else if let string = anyObject as? String {
        if let timeStamp = TimeInterval(string) {
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.full
            property = Date(timeIntervalSince1970: timeStamp)
        }
    }
    return property
}


public func <~ <T>( property: inout [T], object: [String]) {
    var newArray = [T]()
    for element in object {
        var nElement: T?
        nElement <~ element
        if let uElement = nElement {
            newArray.append(uElement)
        }
    }
    property = newArray
}

private func mapToType <T>( _ value: inout T?, anyObject: Any) {
    if let strObject = anyObject as? NSString {
        let type = T.self
        switch type {
            case is Int.Type: value = strObject.integerValue as? T
            case is Double.Type: value = strObject.doubleValue as? T
            case is Float.Type: value = strObject.floatValue as? T
            case is CGFloat.Type: value = (CGFloat(atof(strObject.utf8String))) as? T
            case is Bool.Type: value = strObject.boolValue as? T
        default: break
        }
    }
}



