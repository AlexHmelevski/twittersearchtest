//
//  PhotoCell.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit

class PhotoCell: TweetPlainTextCell {

//    @IBOutlet var avatar: AvatarView!
//    @IBOutlet var tweetText: TweetTextView!
    @IBOutlet var picture: UIImageView!
    var needRemoveMediaObs = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        picture.image = nil
        if needRemoveMediaObs {
            needRemoveMediaObs = false
            assosiatedTweet.removeObserver(self, forKeyPath:  #keyPath(Tweet.entities.media.mediaData))
            
        }
    }
    
    override func set(tweet: Tweet) {
        super.set(tweet: tweet)
        if  tweet.needsUpdate {
            needRemoveMediaObs = true
            assosiatedTweet.addObserver(self, forKeyPath: #keyPath(Tweet.entities.media.mediaData), options: [.new], context: nil)
        } else {
            if let data = tweet.entities.media.mediaData,
                let img = UIImage(data: data) {
                self.picture.image = img
            }
        }
    
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        if let keypath = keyPath {
            switch keypath {
            case #keyPath(Tweet.entities.media.mediaData):
                if let data = change?[NSKeyValueChangeKey.newKey] as? Data,
                    let img = UIImage(data: data) {
                     self.picture.image = img
                }
            default: break
            }
            
        }
    }
}
