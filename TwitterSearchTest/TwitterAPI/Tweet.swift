//
//  Tweet.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit

final class TweetArray : ACMappableProtocol,CustomStringConvertible,Printable {
    var tweets : [Tweet] = []
    init(dict: [String : Any]) {
        tweets <~ (dict["statuses"] ?? ["" : 0])
    }
}


final class Tweet: NSObject, ACMappableProtocol,Printable {
    var user : User = User()
    var entities: Entity = Entity()
    var text : String = ""
    var id: Int = -1
    
    var needsUpdate: Bool {
        if case .Photo = mediaType {
            return user.profile_image == nil || entities.media.mediaData == nil
        }
        return user.profile_image == nil
    }
    
    var mediaType : TweetMediaType {
        return TweetMediaType(rawValue: entities.media.type) ?? .plainText
    }
    
    var containsMedia: Bool {
        if case .Video = mediaType {
            return !entities.media.expanded_url.isEmpty
        }
        return !entities.media.media_url_https.isEmpty
    }
    
    override init() {
        super.init()
    }
    
    init(dict: [String : Any]) {
        super.init()
        user <~ (dict["user"] ?? ["" : 0])
        entities <~ (dict["extended_entities"] ?? ["" : 0])
        text <~ dict["text"]
        id <~ dict["id"]
    }
}


final class User: NSObject, ACMappableProtocol,Printable {
    var name: String = ""
    var profile_image_url_https: String = ""
    dynamic var profile_image: UIImage?

    
    override init() {
        super.init()
    }
    
    init(dict: [String : Any]) {
        super.init()
        name <~ dict["name"]
        profile_image_url_https <~ dict["profile_image_url_https"]
    }
    
}

final class Entity: NSObject, ACMappableProtocol,Printable {
    var mediaArray : [Media] = []
    var hashtag: [Hashtag] = []
    var media: Media = Media()
    override init() {
        super.init()
    }
    init(dict: [String : Any]) {
        super.init()
        mediaArray <~ (dict["media"] ?? ["" : 0])
        hashtag <~ (dict["hashtag"] ?? ["" : 0])
        if mediaArray.count > 0 {
            media = mediaArray.first!
        }
    }
}


enum TweetMediaType: String, CustomStringConvertible {
    case Photo = "photo"
    case Video = "video"
    case plainText = "plainText"
    
    var description: String {
        return self.rawValue
    }
}

final class Media: NSObject, ACMappableProtocol,Printable {
    var type: String = ""
    var media_url_https: String = ""
    var expanded_url: String = ""
    dynamic var mediaData : Data?
    override init() {
        super.init()
    }
    init(dict: [String : Any]) {
        super.init()
        type <~ dict["type"]
        media_url_https <~ dict["media_url_https"]
        expanded_url <~ dict["expanded_url"]
        if expanded_url.contains(TweetMediaType.Video.description) {
            type = TweetMediaType.Video.description
        }
        var video_info: VideoInfo? =  nil
        video_info <~ (dict["video_info"] ?? ["" : 0])
        if let info = video_info, info.variants.count > 0 {
            expanded_url = info.variants.first!.url
        }
    }
}


final class VideoInfo: NSObject, ACMappableProtocol,Printable {
    var variants : [Variant] = []
    
    override init() {
        super.init()
    }
    init(dict: [String : Any]) {
        super.init()
        variants <~ (dict["variants"] ?? ["" : 0])
    }
}


final class Variant: NSObject, ACMappableProtocol,Printable {
    var content_type: String = ""
    var url: String = ""
    
    init(dict: [String : Any]) {
        super.init()
        content_type <~ dict["content_type"]
        url <~ dict["url"]
    }
}


final class Hashtag: NSObject, ACMappableProtocol,Printable {
    var indices: [Int] = []
    var text: String = ""
    init(dict: [String : Any]) {
         super.init()
        indices <~ dict["indices"]
        text <~ dict["dict"]
    }
}
