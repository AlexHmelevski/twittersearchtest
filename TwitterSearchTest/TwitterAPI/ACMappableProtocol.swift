//
//  ACMappableProtocol.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
//public typealias ACWundergroundJSON = Dictionary<String, AnyObject>


public protocol ACMappableProtocol{
    init(dict: [String : Any])
}

public protocol CustomFromStringConvertable {
    init?(_ text: String)
}

public protocol CustomFromStringConvertableInt {
    init?(_ text: String, radix: Int)
}




