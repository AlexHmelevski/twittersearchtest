//
//  AppOnlyAccount.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

struct AppOnlyAccount:CustomStringConvertible,Printable {
    let consumerKey: String
    let secretKey: String
    
    var basicAuthString: String {
        let data = (consumerKey + ":" + secretKey).data(using: .utf8)!
        return "Basic " + data.base64EncodedString()
    }
}


