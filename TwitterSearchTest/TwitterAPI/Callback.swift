//
//  Callback.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

typealias VoidClosure = () -> Void
typealias ErrorClosure = (_ error: Error) -> Void

struct Callback<T> {
    
    private(set) var start: VoidClosure?
    private(set) var success: (_ response: T) -> Void = {response in }
    private(set) var failure: ErrorClosure = {error in }
    private(set) var completed: VoidClosure?
    
    func start(closure: @escaping VoidClosure) -> Callback {
        var callback = self
        callback.start = closure
        return callback
    }
    
    func success(closure: @escaping (_ response: T) -> Void) -> Callback {
        var callback = self
        callback.success = closure
        return callback
    }
    
    func failure(closure: @escaping ErrorClosure) -> Callback {
        var callback = self
        callback.failure = closure
        return callback
    }
    
    func completed(closure: @escaping VoidClosure) -> Callback {
        var callback = self
        callback.completed = closure
        return callback
    }
    
}
