//
//  AuthorizationResponse.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

protocol Printable {
    var description: String {get}
}

extension Printable {
    var description: String {
        var out = String("<\(type(of: self))>: { \n ")!
        for ch in Mirror.init(reflecting: self).children {
            out.append(ch.label ?? "no label")
            out.append(" : ")
            out.append("\(ch.value) \n ")
        }
        out.append("}")
        return out
    }
}

struct AuthorizationResponse: ACMappableProtocol, CustomStringConvertible,Printable {
    private (set) var token: String = ""
    private (set) var tokenType: String = ""
    
    init(dict: [String : Any]) {
        token <~ dict["access_token"]
        tokenType <~ dict["token_type"]
    }
}
