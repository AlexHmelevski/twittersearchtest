//
//  NetworkRequest.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

enum NetworkMethod: String {
    case Post = "POST"
    case Get = "GET"
    
}


final class SerializedNetwork: Network {
    
    func loadData<T: ACMappableProtocol>(fromURLString string : String, callback: Callback<T>) {
        
        let serializedCallback = Callback<Data>()
        .start {
            callback.start?()
        }
        .success { (data) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : Any]
                let object = T.init(dict: json)
                callback.success(object)
                
            } catch  {
                callback.failure(error)
                callback.completed?()
            }
            
        }
        .failure { (error) in
            callback.failure(error)
            
        }
        .completed {
            callback.completed?()
        }
        
        super.loadData(fromURLString: string, callback: serializedCallback)
        
    }
}


class Network {
    private let method: NetworkMethod
    var headers: [Header]
    private var body: Data?
    var session: URLSession = URLSession(configuration: URLSessionConfiguration.default)
    
    init(method: NetworkMethod = .Get, headers: [Header] = [], body: Data? = nil) {
        self.method = method
        self.headers = headers
        self.body = body
    }
    
    func loadData(fromURLString string : String, callback: Callback<Data>) {
    
       let request = self.createURLRequest(fromURlString: String(describing: string.utf8))
        print(request)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let uError = error {
                DispatchQueue.main.async {
                    callback.failure(uError)
                    callback.completed?()
                }
                
                return
            }
            if let uData = data {
                DispatchQueue.main.async {
                    callback.success(uData)
                    callback.completed?()
                }
                
                return
            }
        }
        callback.start?()
        task.resume()
       
    }
    
    private func  createURLRequest(fromURlString string: String ) -> URLRequest {
        guard let url = URL(string: string) else {
            fatalError("Can't create URL from String")
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = method.rawValue
        
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        if let body = self.body {
            request.httpBody = body
        }
        
        return request
    }
}
