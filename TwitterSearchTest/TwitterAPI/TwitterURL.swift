//
//  TwitterURL.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

enum TwitterURL {
    case Auth
    case HashtagSearch
    
    var urlString: String {
        var str = ""
        switch self {
        case .Auth: str = "https://api.twitter.com/oauth2/token"
        case .HashtagSearch: str = "https://api.twitter.com/1.1/search/tweets.json?q="
        }
        
        return str
    }
    
    
    var url: URL {
        return URL(string: urlString)!
    }
}
