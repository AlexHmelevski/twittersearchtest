//
//  TweetMediaReader.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit

protocol TweetMediaReadProtocol {
    func loadProfilePicture(inTweet tweet: Tweet, callback: Callback<Tweet>)
    func loadMedia(inTweet tweet: Tweet,callback: Callback<Tweet> )
}

final class TweetMediaReader: TweetMediaReadProtocol {
    var network: SerializedNetwork = SerializedNetwork()
    
    func loadMedia(inTweet tweet: Tweet, callback: Callback<Tweet>) {
        callback.start?()
        if !tweet.containsMedia {
            callback.completed?()
            return
        }
        
        let mediaCallback = Callback<Data>()
            .success { (data) in
                tweet.entities.media.mediaData = data
                callback.success(tweet)
            }
            .failure { (error) in
                callback.failure(error)
            }
            .completed {
                callback.completed?()
                
        }
        
        network.loadData(fromURLString: tweet.entities.media.media_url_https, callback: mediaCallback)
    }
    
    func loadProfilePicture(inTweet tweet: Tweet, callback: Callback<Tweet>) {
        let mediaCallback = Callback<Data>()
            .start {
                
            }
            .success { (data) in
                
                if let img = UIImage(data: data) {
                    if tweet.user.profile_image == nil {
                        tweet.user.profile_image = img
                    }
                    
                }
                
            }
            .failure { (error) in
                
            }
            .completed {
                
        }
        
        network.loadData(fromURLString: tweet.user.profile_image_url_https, callback: mediaCallback)
    }
}
