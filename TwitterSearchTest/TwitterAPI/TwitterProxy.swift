//
//  TwitterProxy.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation


protocol ITwitterProxy {
    func getToken(callback: Callback<AuthorizationResponse>)
}


final class TwitterProxy: ITwitterProxy {
    
    private var key: AuthorizationResponse?
    private let account : AppOnlyAccount
    
    init(account: AppOnlyAccount = AppOnlyAccount(consumerKey: "BwvGeTvii49AV6kJxUC8Sw", secretKey: "vm1q1VrH8lUfm8VQipNmI0qxFQKvvtOPS7rjcjcSOVA")) {
        self.account = account
    }
    
    func getToken(callback: Callback<AuthorizationResponse>) {
        
        callback.start?()
        if let key = key {
            callback.success(key)
            callback.completed?()
        } else {
            let client = SerializedNetwork.init(method: .Post,
                                                headers: [.Authorization(account.basicAuthString)],
                                                body: Header.Grant_Type("client_credentials").data)
            
            let tCallback = Callback<AuthorizationResponse>()
                .success { [weak self] (response) in
                    self?.key = response
                    callback.success(response)
                }
                .failure { (error) in
                    callback.failure(error)
                }
                .completed {
                    callback.completed?()
            }
            
            client.loadData(fromURLString: TwitterURL.Auth.urlString, callback: tCallback)
        }
    }
}
