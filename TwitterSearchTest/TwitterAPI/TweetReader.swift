//
//  TwitterClient.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation

protocol TweetSearchProtocol {
    func search(byHashtag: String, callback: Callback<TweetArray>)
}

final class TweetReader: TweetSearchProtocol {
    var network: SerializedNetwork = SerializedNetwork()
    private let proxy : ITwitterProxy
    private var group = DispatchGroup()
    private let backgroundQ = DispatchQueue(label: "twitter.test.search")
    
    
    init(proxy: ITwitterProxy) {
        self.proxy = proxy
    }
    
    internal func search(byHashtag: String, callback: Callback<TweetArray>) {
        callback.start?()
                
        self.group.enter()
        backgroundQ.async {[weak self ] in
            self?.proxy.getToken(callback: Callback<AuthorizationResponse>()
            .success(closure: { (token) in
                    self?.network.headers = [.Authorization((token.tokenType + " " + token.token))]
                })
            .completed {
                self?.group.leave()
            })

        }
        let modified = callback.start{}
        let tags = byHashtag.components(separatedBy: "+").map { (tag) -> String in
            return ("#" + tag).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        }
        self.group.notify(queue: .main, execute: {
            self.network.loadData(fromURLString: TwitterURL.HashtagSearch.urlString + tags.joined(separator: "+") + "&count=50", callback: modified)
        })
    }
}
