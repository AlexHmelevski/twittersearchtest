//
//  HeaderEnum.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation


enum Header  {
    case Authorization(String)
    case Content_Type(String)
    case Host(String)
    case Grant_Type(String)
    
    var key: String {
        var key = ""
        switch self {
        case .Authorization: key = "Authorization"
        case .Content_Type: key = "Content-Type"
        case .Host: key = "Host"
        case .Grant_Type: key = "grant_type"
        }
        return key
    }
    
    var value: String {
        var value = ""
        switch self {
        case .Authorization(let v) : value = v
        case .Content_Type(let v): value = v
        case .Grant_Type(let v): value = v
        case .Host(let v): value = v
        }
        return value
    }
    
    var data: Data {
        return (key + "=" + value).data(using: .utf8)!
    }
}
