//
//  TweetTextView.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit

class TweetTextView: UITextView {

    var formatter: ITweetTextFormatProtocol = TweetHashtagFormatter()

    override var text: String! {
        didSet {
            attributedText = self.formatter.format(text: text)
        }
    }
    
    
    
}
