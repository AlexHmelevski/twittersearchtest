//
//  AvatarView.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit

class AvatarView: UIView {
    
    var placeHolder: UIView = UIView(frame: .zero)
    var imageView: UIImageView = UIImageView(frame: .zero)
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLayout()
    }
    
    
    private func setUpLayout() {
        addSubview(imageView)
        addSubview(placeHolder)
        placeHolder.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        self.clipsToBounds = true
        initConstraints(forView: imageView)
        initConstraints(forView: placeHolder)
        
    }
    
    func setImage(image: UIImage?) {
        if let _ = image {
            placeHolder.isHidden = true
        } else {
            placeHolder.isHidden = false
        }
        imageView.image = image
    }
    
    func addPlaceholder(view: UIView) {
    
        placeHolder.addSubview(view)
        initConstraints(forView: view)
    }
    
    private func initConstraints(forView view: UIView) {
        NSLayoutConstraint.constraints(withVisualFormat: "V:[view]", options: .alignAllCenterX, metrics: nil, views: ["view": view])
        NSLayoutConstraint.constraints(withVisualFormat: "H:[view]", options: .alignAllCenterY, metrics: nil, views: ["view": view])
    }
    
}
