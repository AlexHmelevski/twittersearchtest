//
//  TweetsDataSource.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit

class TweetsDataSource: NSObject, UITableViewDataSource {
    
    var tweetReader: TweetSearchProtocol!
    var tweetMediaReader: TweetMediaReadProtocol!
    weak var tableView: UITableView!
    var tweets: [Tweet] = []
    
    override init () {
        super.init()
        let account = AppOnlyAccount(consumerKey: "BwvGeTvii49AV6kJxUC8Sw", secretKey: "vm1q1VrH8lUfm8VQipNmI0qxFQKvvtOPS7rjcjcSOVA")
        
        let twClient = TwitterProxy(account: account)
        tweetReader = TweetReader(proxy: twClient)
        tweetMediaReader = TweetMediaReader()
    }
    
    func associateTableView(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.dataSource = self
        self.tableView.register(CellFactory.cellNib(forType: .plainText), forCellReuseIdentifier: CellFactory.reuseIdentifier(forType: .plainText))
        self.tableView.register(CellFactory.cellNib(forType: .Photo), forCellReuseIdentifier: CellFactory.reuseIdentifier(forType: .Photo))
         self.tableView.register(CellFactory.cellNib(forType: .Video), forCellReuseIdentifier: CellFactory.reuseIdentifier(forType: .Video))
        self.tableView.estimatedRowHeight = 85
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tweet = tweets[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellFactory.reuseIdentifier(forType: tweet.mediaType), for: indexPath) as! TweetTableViewCell
        cell.set(tweet: tweet)
        if tweet.needsUpdate {
            tweetMediaReader.loadProfilePicture(inTweet: tweet, callback: Callback<Tweet>())
            tweetMediaReader.loadMedia(inTweet: tweet, callback: Callback<Tweet>())
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets.count
    }
    
    
    func fetchData(forString query: String) {
        let tweetCallback = Callback<TweetArray>()
            .success { [weak self] (array) in
                self?.tweets = array.tweets
                self?.tableView.reloadData()
            }
            .failure { (error) in
                print(error)
        }
        
        tweetReader.search(byHashtag: query, callback: tweetCallback)
    }
}


extension TweetsDataSource: SearchEnginePeer {
    
    
    func startSearch(string: String) {
        resetCells()
        fetchData(forString: string)
    }
    
    private func resetCells() {
        for (idx,tweet) in tweets.enumerated() {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellFactory.reuseIdentifier(forType: tweet.mediaType), for: IndexPath(row: idx, section: 0))
            cell.prepareForReuse()
        }
    }
}
