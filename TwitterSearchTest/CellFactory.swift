//
//  CellFactory.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit

class CellFactory {
    
    class func cellNib(forType type: TweetMediaType) -> UINib {
        return UINib(nibName: self.reuseIdentifier(forType: type), bundle: nil)
        
    }
    
    class func reuseIdentifier(forType type: TweetMediaType) -> String {
        switch type {
        case .Photo: return "PhotoCell"
        case .Video: return "VideoCell"
        default: return "PlainTextCell"
        }
        
    }
}
