//
//  TweetPlainTextCell.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {
    func set(tweet: Tweet) {
        fatalError("Subclasses must override")
    }
}

private var context = 0

class TweetPlainTextCell: TweetTableViewCell {

    @IBOutlet var avatar: AvatarView!
    @IBOutlet var tweetText: TweetTextView!
    var assosiatedTweet: Tweet!
    var needRemoveAvatarObs = false

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeUIElements()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        avatar.setImage(image: nil)
        if needRemoveAvatarObs {
          assosiatedTweet.removeObserver(self, forKeyPath: #keyPath(Tweet.user.profile_image))
            needRemoveAvatarObs = false
        }
        
    }

    override func set(tweet: Tweet) {
        assosiatedTweet = tweet
        tweetText.text = tweet.text
        if let image =  tweet.user.profile_image {
           avatar.setImage(image: image)
        } else {
            needRemoveAvatarObs = true
            assosiatedTweet.addObserver(self, forKeyPath: #keyPath(Tweet.user.profile_image), options: [.new], context: &context)
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if let keypath = keyPath {
                switch keypath {
                case #keyPath(Tweet.user.profile_image):
                    if let img = change?[NSKeyValueChangeKey.newKey] as? UIImage {
                        avatar.setImage(image: img)
                    }
                default: break
                }
    
            }
    }
    
    private func initializeUIElements() {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.startAnimating()
        avatar.addPlaceholder(view: indicator)
    }

    
    override class var layerClass: Swift.AnyClass {
        return CALayer.self
    }
    

}
