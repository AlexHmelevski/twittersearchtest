//
//  VideoCell.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoCell: TweetPlainTextCell {

    @IBOutlet var videoContainter: UIView!
    private var tmpVideoView : UIView = UIView(frame: .zero)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        tmpVideoView.removeFromSuperview()
    }
    override func set(tweet: Tweet) {
        super.set(tweet: tweet)
        if case .Video = tweet.mediaType {
            let vController = AVPlayerViewController()
            let url = URL(string: tweet.entities.media.expanded_url)!
            vController.player = AVPlayer(url: url)
            tmpVideoView = vController.view
            tmpVideoView.frame = videoContainter.bounds
            videoContainter.addSubview(tmpVideoView)
        }
        
    }
}
