//
//  ViewController.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-10.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    var dataSource = TweetsDataSource()
    var searchBarSource = SearchBarManager()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.associateTableView(tableView: self.tableView)
        searchBarSource.associate(searchBar: searchBar)
        searchBarSource.registerPeer(peer: dataSource)
        
        tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(searchBarSource, action: #selector(SearchBarManager.startSearch), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
}



