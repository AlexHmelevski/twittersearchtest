//
//  TweetTextFormatter.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit
protocol ITweetTextFormatProtocol {
    func format(text: String) -> NSAttributedString
}


final class TweetHashtagFormatter: ITweetTextFormatProtocol {
    var color: UIColor = .red
    
    func format(text: String) -> NSAttributedString {
        let atString = NSMutableAttributedString(string: text)
        let nsString = NSString(string: text)

       let words = nsString.components(separatedBy: " ")
        
        for word in words {
            if word.contains("#") {
                atString.addAttribute(NSForegroundColorAttributeName, value: self.color, range: nsString.range(of: word))
            }
        }
        

        return atString
    }
}
