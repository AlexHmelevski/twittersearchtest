//
//  SearchBarManager.swift
//  TwitterSearchTest
//
//  Created by Alex Crow on 2016-12-11.
//  Copyright © 2016 AlexCrowe. All rights reserved.
//

import Foundation
import UIKit

protocol SearchEnginePeer {
    var hashValue: Int { get }
    func startSearch(string: String)
}

protocol SearchEngineMediator {
    func registerPeer(peer: SearchEnginePeer)
    func removePeer(peer: SearchEnginePeer)
    func startSearch()
}

final class SearchBarManager: NSObject, UISearchBarDelegate {
    
    fileprivate var peers: [Int: SearchEnginePeer] = [:]
    var query: String = ""
    
    func associate(searchBar: UISearchBar) {
        searchBar.delegate = self
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            query = text
            startSearch()
        }
        searchBar.resignFirstResponder()
    }
}

extension SearchBarManager: SearchEngineMediator {
    func registerPeer(peer: SearchEnginePeer) {
        peers[peer.hashValue] = peer
    }
    
    
    func removePeer(peer: SearchEnginePeer) {
       peers.removeValue(forKey: peer.hashValue)
    }
    
    func startSearch() {
      peers.values.forEach({$0.startSearch(string: query)})
    }
}
